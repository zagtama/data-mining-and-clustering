# Import module and lib
import pandas as pd                     # Lib pembacaaan dataframe
import matplotlib.pyplot as plt         # Lib ploting data dalam bentuk 2D
import numpy as np                      # Lib vektor dan matriks
import scipy.stats as stats             # Lib statsitik data
import seaborn as sns                   # Lib visualisasi data statistik
from matplotlib import rcParams
import statsmodels.api as sm
from statsmodels.formula.api import ols

if __name__ == "__main__":
    # Print dataset 
    df = pd.read_csv('kc_house_data.csv')
    print('[Print dataset]')
    print(df.head())
    print('\n[Check if data has null value]')
    print(df.isnull().any())
    print('\n[Check data type]')
    print(df.dtypes)
    print('\n[Describe dataset]')
    print(df.describe())

    # Plotting dataset
    fig = plt.figure(figsize=(12,6))
    sqft = fig.add_subplot(121)
    cost = fig.add_subplot(122)

    sqft.hist(df.sqft_living, bins=80)
    sqft.set_xlabel('Ft^2')
    sqft.set_title("Histogram of House Square Footage")

    cost.hist(df.price, bins=80)
    cost.set_xlabel('Price ($)')
    cost.set_title("Histogram of Housing Prices")

    plt.show()
    print()

    m = ols('price ~ sqft_living',df).fit()
    print (m.summary())
    print()

    m = ols('price ~ sqft_living + bedrooms + grade + condition',df).fit()
    print (m.summary())
    print()

    sns.jointplot(x="sqft_living", y="price", data=df, kind = 'reg',fit_reg= True, height = 7)
    plt.show()